System sklada sie z dwoch mikrokontrolerow. Mastera AT89C51-24JC do ktorego podlaczona jest klawiatura i wyswietlacz LCD oraz Slavea TINY26LP do ktorego podlaczone sa czujniki. Mikrokontrolery komunikuja sie co pewien czas przez port szeregowy. Polaczenie wymuszane jest przez Mastera. Jesli ktorys z nich nie otrzyma informacji o dzialaniu drugiego w wyznaczonym czasie wlaczany jest alarm. Ta droga przekazywane sa rowniez dane takie jak sygnaly sterujace. Slave, jesli ustawione sa flagi OFF/ON(dzialanie ukladu) i SEN_ON (zbieranie informacji z czujnikow) zbiera informacje z czujnikow, zapisuje ich stan do tablicy i w razie wykrycia intruza ustawia flage WARNING. Informacja o tym jest przekazywana do Mastera. Flaga Warning moze byc zdjeta tylko po odebraniu sygnalu WARN_OFF od Mastera w wyznaczonym, wczesniej ustawionym przez uzytkownika czasie. W przeciwnym razie wlaczany jest alarm. Master co jakis czas testuje takze stan klawiatury. W celu zapobiegania bledom wykrycia niewlasciego wcisnietego przycisku spowodowanym drganiem na stykach, klawisz uwaza sie za wcisniety/wycisniety jesli zarejestrowany zostanie dwa razy pod rzad stan stabilny inny niz poprzedni stan stabilny klawiatury. Wciskajac roznorodne klawisze wymuszamy na systemie pozadane dzialanie: 
-Wlaczanie i poruszanie sie po menu (wejscie do menu po podaniu kodu)
-wpisywanie kodu(w tym wylaczanie alarmu i stanu WARNING)
-wprowadzanie ustawien (aktualny czas, nowy kod, czas na rekacje, czas wlaczenia czujnikow)
Ustawiane sa wtedy roznorodne flagi. Dzieki nim system "wie" gdzie uzytkownik sie znajduje, ile cyfr kodu juz wpisal itd. W zaleznosci od tego jaka akcja zostala wykonana badz nie (jakie flagi sa aktualnie ustawione) wysylane sa sygnaly sterujace do Slavea z odpowiednimi wartosciami (wylacz sie-OFF, wylacz czujniki-SEN_ON, wylacz ostrzezenie-WARN_OFF). Master pobiera takze stan czujnikow. Na wyswietlaczu wyswietlane sa odpowiednie komunikaty. Jesli system jest wlaczony ale nikt nie wprowadza zmian (stan klawiatury IDLE wlaczany po 15 sekundach od ostatniego uzycia) wyswietlany jest zegar i flagi. Inne mozliwe dialogi wyswietlane na wyswietlaczu to:
-pozycje w menu, w tym aktualnie wybrana
-wpisywanie kodu
-dialogi poszczegolnych ustawien
-w przypadku stanu ostrzezenia i alarmu stan tablicy czujnikow (ktore czujniki wykryly intruza)
Klawiatura sklada sie z:
-10 przyciskow cyfr do wprowadzania danych (kod, ustawienia)
-dwoch przyciskow nawigacji
-przycisku enter do zatwierdzania kodu i ustawien oraz wyboru pozycji w menu
-przycisku back do cofania wprowadznych wartosci badz wyjscie do menu nadrzednego jesli przytrzymany jest przez co najmniej 3 sekundy
-przycisku lock/unlock do wprowadzenia kodu
-przycisku menu